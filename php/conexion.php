<?php
class mybdenmysql{
  var $host_db;
  var $user_db;
  var $pass_db;
  var $db_name;
  var $enlace;

  function mybdenmysql(){
     $this->host_db = "localhost";
     $this->user_db = "root";
     $this->pass_db = "usbw";
     $this->db_name = "optica-ingsfw";
  }
  function conectar(){
    $this->enlace = new mysqli($this->host_db, $this->user_db, $this->pass_db, $this->db_name);
    if ($this->enlace->connect_error) {
      die("La conexion falló: " . $this->enlace->connect_error);
    }
  }

  function obtener_consulta($consulta) {
    $resultado=$this->enlace->query($consulta);
    if($resultado->num_rows!=1){
      $resultado=false;
    }
    return $resultado;
  }

  function desconectar() {
    mysql_close($this->enlace);
  }

}
?>
