<?php
  session_start();
?>
<?php
include('/php/conexion.php');
$cargo="";


function verificar_login($user,$password,&$result)
    {
      $mysql=new mybdenmysql();
      $mysql->mybdenmysql();
      $mysql->conectar();
      $sql = "SELECT `nombre_usuario`,`contrasena_usuario`,`cargo_usuario` FROM `usuario` WHERE `nombre_usuario`= \"$user\" && `contrasena_usuario` = \"$password\"";
      $resconsulta=$mysql->obtener_consulta($sql);
      if($resconsulta!=false){
        $row = $resconsulta->fetch_array(MYSQLI_ASSOC);
        $cargo=$row['cargo_usuario'];
        $result=$row;
        return 1;
      }else{
        return 0;
      }
      $mysql->desconectar();
    }

 ?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8"/>
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Acceso</title>
  </head>
  <body class="nav-wrapper pink lighten-3">
    <div class="section"></div>
      <main>
        <center>
          <div class="section"></div>
          <h5 class="indigo-text">Porfavor primero autenticar </h5>
          <div class="section"></div>
          <div class="container">
            <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">
              <?php
              if(!isset($_SESSION['username']))  {
                if(isset($_POST['login'])){
                  if(verificar_login($_POST['username'],$_POST['password'],$result) == 1) {
                    $_SESSION['loggedin'] = true;
                    $_SESSION['username'] = $user;
                    $_SESSION['start'] = time();
                    $_SESSION['expire'] = $_SESSION['start'] + (5 * 60);
                    if($result['cargo_usuario']=="Secretario" || $result['cargo_usuario']=="Secretaria"){
                      header("Location: /principal.html");
                    }else{
                      header("Location: /formulario_medico.html");
                    }
                  }
                  else
                  {
                    echo '<div class="error">Su usuario es incorrecto, intente nuevamente.</div>';
                  }
                }
                ?>
              <form action="" method="post" class="login">
                <div class='row'>
                  <div class='col s12'></div>
                </div>
                <div class='row'>
                  <div class='input-field col s12'>
                    <input class='validate' type="text" name="username" id='username' />
                    <label for='email'>Ingrese su usuario</label>
                  </div>
                </div>
                <div class='row'>
                  <div class='input-field col s12'>
                    <input class='validate' type="password" name="password" id='password' />
                    <label for='password'>Ingrese su contraseña</label>
                  </div>
                  <label style='float: right;'>
                    <a class='pink-text' href='#!'><b>Olvidó su contraseña?</b></a>
                  </label>
                </div>
                <br/>
                <center>
                  <div class='row'>
                    <button name="login" class='col s12 btn btn-large waves-effect indigo'>Login</button>
                  </div>
                </center>
              </form>
              <?php
            } else {
              // Si la variable de sesión ‘userid’ ya existe, que muestre el mensaje de saludo.
              echo 'Su usuario ingreso correctamente.';
              echo '<a href="logout.php">Logout</a>';
            }
            ?>

            </div>
          </div>
          <a href="#!"></a>
        </center>
        <div class="section"></div>
        <div class="section"></div>
      </main>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
    </body>
</html>
